# Top 5 Rows of Dataset
data.head()

# Last 5 Rows of Dataset
data.tail()

# Shape of the Dataset
data.shape

# Description of the dataset
data.describe()

# List of column names
data.columns

# Cheking the dataset information
data.info()


